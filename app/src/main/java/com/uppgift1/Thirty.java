package com.uppgift1;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Random;

public class Thirty {

    // Constants
    private static final int mNrOfRounds = 10;
    private static final int mNrOfReThrowsPerRound = 2;
    private static final int mNrOfDice = 6;

    // Variables
    private Die[] mDice;
    private int[] mRoundSums;
    private int mNrOfReThrows;
    private int mNrOfRoundsComplete;
    private boolean mGameOver;
    Random mRng;

    public Thirty() {
        mDice = new Die[mNrOfDice];
        mRoundSums = new int[mNrOfRounds];
        for(int i = 0; i < mNrOfDice; i++) {
            mDice[i] = new Die();
        }

        mRng = new Random();

        reset();
    }

    public void reset() {
        int tempFaceValue = 0;
        for(Die d: mDice) {
            tempFaceValue = mRng.nextInt(6);
            tempFaceValue++;
            d.setFaceValue(tempFaceValue);
        }

        for(int i = 0; i < mNrOfRounds; i++) {
            mRoundSums[i] = 0;
        }

        mNrOfReThrows = 0;
        mNrOfRoundsComplete = 0;
        mGameOver = false;
    }

    // Throws all the unsaved dice
    public void throwDice(Context context) {
        if(mNrOfReThrows < mNrOfReThrowsPerRound) {
            int tempFaceValue = 0;
            mNrOfReThrows++;
            for(Die d: mDice) {
                if(!d.isSaved()) {
                    tempFaceValue = mRng.nextInt(6); // Random value 0-5
                    tempFaceValue++; // Make it in the range of 1-6
                    d.setFaceValue(tempFaceValue);
                }
            }
        }
        else {
            Toast.makeText( context,
                    context.getString(R.string.throw_error),
                    Toast.LENGTH_SHORT).show();
        }
    }

    // Handles
    public boolean completeRound(Context context, int selectedSpinnerID) {
        if(mRoundSums[selectedSpinnerID] != 0) { // Error: A value is already saved there
            Toast.makeText( context,
                            context.getString(R.string.complete_round_error),
                            Toast.LENGTH_SHORT).show();
            return false;
        }
        else {
            if(selectedSpinnerID > 0) {
                mRoundSums[selectedSpinnerID] = calculateSum(selectedSpinnerID + 3);
            }
            else {
                mRoundSums[selectedSpinnerID] = calculateLowSum();
            }

            mNrOfRoundsComplete++;
            if(mNrOfRoundsComplete == mNrOfRounds) {
                mGameOver = true;
            }

            // Give dice new face values
            int tempFaceValue = 0;
            mNrOfReThrows++;
            for(Die d: mDice) {
                tempFaceValue = mRng.nextInt(6); // Random value 0-5
                tempFaceValue++; // Make it in the range of 1-6
                d.setFaceValue(tempFaceValue);
            }

            Log.d("Thirty", "Sum " + selectedSpinnerID +": " + mRoundSums[selectedSpinnerID]);
            return true;
        }
    }

    // Returns the face value of a die with the index "dieIndex"
    public int getValueFromDie(int dieIndex) {
        return mDice[dieIndex].getFaceValue();
    }

    // Handles pressed die
    public void diePressed(int dieIndex) {
        mDice[dieIndex].setSaved(!mDice[dieIndex].isSaved()); // Inverts saved value
    }

    // Returns the save state of die with index "dieIndex"
    public boolean isDieSaved(int dieIndex) {
        return mDice[dieIndex].isSaved();
    }

    // Return nr of dice
    public int getNrOfDice() {
        return mNrOfDice;
    }

    // Adds together all the low values(1-3) and return the sum
    private int calculateLowSum() {
        int sum = 0, temp = 0;

        for(Die d: mDice) {
            temp = d.getFaceValue();
            if(temp < 4)
                sum += temp;
        }

        return sum;
    }

    // Calculate the most number of unique combinations to add up to a specific number
    // from the current die combination.
    // Category is the specific number
    private int calculateSum(int category) {
        int sum = 0, i = 0;
        ArrayList<Integer> tempDice = new ArrayList<Integer>();

        // Add faceValues to tempDice and check if they are equal or larger to category
        // If they where equal remove from tempDice and add it to the sum
        // If they where larger remove from tempDice
        for(Die d: mDice) {
            tempDice.add(d.getFaceValue());
            if(tempDice.get(i).intValue() == category) {
                sum++;
                tempDice.remove(i);
            }
            else if(tempDice.get(i).intValue() > category) {
                tempDice.remove(i);
            }
            else {
                i++;
            }
        }

        // Fix score
        sum *= category;

        // Find most number of unique combinations
        sum += findMostNrOfUniqueCombinationsToAddUpToNr(tempDice, category, 0) * category;

        return sum;
    }


    private int findMostNrOfUniqueCombinationsToAddUpToNr(ArrayList<Integer> numbers, int category, int currentSum) {
        int sum = 0, temp = 0, tempSum = 0, bestSum = 0, tempCurSum = 0;

        // Brute force comparison looking at all possible combinations and returning the best result
        for(int i = 0; i < numbers.size(); i++) {
            for(int j = i + 1; j < numbers.size(); j++) {
                tempCurSum = 0;
                ArrayList<Integer> remaining = new ArrayList<>(numbers); // Create new arrayList to be passed on
                temp = numbers.get(i).intValue() + numbers.get(j).intValue();

                if(temp == category) {
                    tempCurSum++;
                    remaining.remove(i);
                    remaining.remove(j - 1);
                }
                else if(temp > category) {
                    remaining.remove(i);
                }
                else {
                    remaining.remove(i);
                    remaining.add(i, temp);
                    remaining.remove(j);
                }

                tempSum = findMostNrOfUniqueCombinationsToAddUpToNr(remaining, category, tempCurSum);
                if(tempSum > bestSum) {
                    bestSum = tempSum;
                }
            }
        }

        sum += bestSum + currentSum;
        return sum;
    }
}
