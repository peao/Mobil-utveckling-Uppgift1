package com.uppgift1;

public class Die {

    // Variables
    private int mFaceValue; // Die value
    private boolean mSaved;

    // Constructors
    public Die() {
        mFaceValue = 1;
        mSaved = false;
    }

    public Die(int faceValue) {
        mFaceValue = faceValue;
        mSaved = false;
    }

    public Die(boolean saved) {
        mFaceValue = 1;
        mSaved = saved;
    }

    public Die(int faceValue, boolean saved) {
        mFaceValue = faceValue;
        mSaved = saved;
    }

    public int getFaceValue() {
        return mFaceValue;
    }

    // Getters and setters
    public void setFaceValue(int faceValue) {
        if(faceValue < 1)
            mFaceValue = 1;
        else if(faceValue > 6)
            mFaceValue = 6;
        else
            mFaceValue = faceValue;
    }

    public boolean isSaved() {
        return mSaved;
    }

    public void setSaved(boolean saved) {
        mSaved = saved;
    }
}
