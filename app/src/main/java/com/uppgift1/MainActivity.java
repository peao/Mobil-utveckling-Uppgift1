package com.uppgift1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity"; // Log tag

    private int mNrOfDice; // The total number of dice
    private Button[] mDiceButtons;
    private Button mThrowButton, mDoneButton;
    private Spinner mScoreChoiceSpinner;
    private Thirty mThirty; // The game

    private void initiateButtons() {
        mDiceButtons = new Button[6];
        // Getting the Button for each die
        mDiceButtons[0] = (Button)findViewById(R.id.dice0_button);
        mDiceButtons[1] = (Button)findViewById(R.id.dice1_button);
        mDiceButtons[2] = (Button)findViewById(R.id.dice2_button);
        mDiceButtons[3] = (Button)findViewById(R.id.dice3_button);
        mDiceButtons[4] = (Button)findViewById(R.id.dice4_button);
        mDiceButtons[5] = (Button)findViewById(R.id.dice5_button);

        // Add listeners to the dice buttons
        for(int i = 0; i < mNrOfDice; i++) {
            final int diceID = i;
            mDiceButtons[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "Die " + diceID + " is pressed");

                    mThirty.diePressed(diceID);

                    // Change image
                    // Calculate id for the image by taking the id of the first image(grey or white)
                    // Then add the faceValue of the dice(minus 1) to get to the appropriate id
                    if(mThirty.isDieSaved(diceID))
                        mDiceButtons[diceID].setBackgroundResource(R.drawable.grey1 + (mThirty.getValueFromDie(diceID) - 1));
                    else
                        mDiceButtons[diceID].setBackgroundResource(R.drawable.white1 + (mThirty.getValueFromDie(diceID) - 1));
                }
            });
        }

        // Getting the button throw and setting listener
        mThrowButton = (Button)findViewById(R.id.throw_button);
        mThrowButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mThirty.throwDice(v.getContext());
                updateDiceButtons();
            }
        });

        // Getting the button done and setting listener
        mDoneButton = (Button)findViewById(R.id.done_button);
        mDoneButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
               if(mThirty.completeRound(v.getContext(), mScoreChoiceSpinner.getSelectedItemPosition())){

               }
                //updateDiceButtons();
            }
        });
        updateDiceButtons();
    }

    private void updateDiceButtons() {
        for(int i = 0; i < mNrOfDice; i++) {
            if(mThirty.isDieSaved(i))
                mDiceButtons[i].setBackgroundResource(R.drawable.grey1 + (mThirty.getValueFromDie(i) - 1));
            else
                mDiceButtons[i].setBackgroundResource(R.drawable.white1 + (mThirty.getValueFromDie(i) - 1));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initiate the game
        mThirty = new Thirty();
        mNrOfDice = mThirty.getNrOfDice();

        initiateButtons();

        // Getting the spinner for the scoreChoiceSpinner
        mScoreChoiceSpinner = (Spinner)findViewById(R.id.scoreChoice_spinner);

        Log.d(TAG, "onCreate()");
    }
}
