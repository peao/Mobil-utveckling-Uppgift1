package com.uppgift1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class EndActivity extends AppCompatActivity {

    private static final String TAG = "EndActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end);

        Log.d(TAG, "onCreate()");
    }
}
